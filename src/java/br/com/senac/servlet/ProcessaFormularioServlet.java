
package br.com.senac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
    1 - Ter uma container servlet 
    2 - extender  HttpServlet
    3 - Mappear o Servlet ( via web.xml ou via anotações)
    web.xml Java EE 5 menor
    web.xml ou anotação JAVA EE 6 ou superior


*/
@WebServlet(urlPatterns = "/processa.do" )
public class ProcessaFormularioServlet  extends HttpServlet{

    @Override
    protected void doGet(
            HttpServletRequest requisao,
            HttpServletResponse resposta) throws ServletException, IOException {
     
        String nome  =  requisao.getParameter("nome") ; 
        
        
        PrintWriter saida  =  resposta.getWriter() ; 
        saida.print("<html>");
        saida.print("<body>");
       
                saida.print("Processado !!!!<br />");
                saida.print("Sr. " + nome);
       
        saida.print("</body>");
        saida.print("</html>");
        
        
    }
    
}
